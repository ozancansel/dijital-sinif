#ifndef SURFACEWIDGET_H
#define SURFACEWIDGET_H

#include <QQuickPaintedItem>
#include <QPainter>
#include <QObject>
#include <rfb/rfbclient.h>
#include <rfb/keysym.h>
#include "rfbclientdelegate.h"

class VncWidget : public QQuickPaintedItem
{

    Q_OBJECT
    Q_PROPERTY(RfbClientDelegate* delegate READ delegate WRITE setDelegate NOTIFY delegateChanged)

public:

    static void         registerQmlType();
    VncWidget(QQuickItem* parent = nullptr);
    void                paint(QPainter* painter);
    RfbClientDelegate*  delegate();
    void                setDelegate(RfbClientDelegate* val);
    QPixmap*            surfacePixmap();
    bool                eventFilter(QObject *watched, QEvent *event);
    void                mousePressEvent(QMouseEvent *event);
    void                mouseReleaseEvent(QMouseEvent *event);
    void                mouseMoveEvent(QMouseEvent *event);
    void                keyPressEvent(QKeyEvent *event);
    void                keyReleaseEvent(QKeyEvent *event);

signals:

    void        delegateChanged();

private slots:

    void        updateFrame();

private:

    rfbClient   *m_client;
    RfbClientDelegate*  m_delegate;
    QPixmap*    m_surfacePixmap;
    QPainter    m_painter;
    int         m_buttonMask;

    rfbKeySym qt2keysym(int qtkey);
    int translateMouseButton(Qt::MouseButton button);

};

#endif // SURFACEWIDGET_H
