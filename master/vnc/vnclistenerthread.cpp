#include "vnclistenerthread.h"
#include "rfbclientdelegate.h"

QMutex VncListenerThread::m_mutex;

//Static
QMutex& VncListenerThread::mutex(){
    return m_mutex;
}

VncListenerThread::VncListenerThread(QObject* parent) :
        QThread(parent) ,
        m_delegate(nullptr)
{
}

void VncListenerThread::run(){
    while(true){
        if(!m_delegate){
            QThread::msleep(16);
            continue;
        }
        if(mutex().tryLock(0)){
            int n = WaitForMessage(m_delegate->client() , 500);
            if(n < 0){

            } else if(n > 0){
                HandleRFBServerMessage(m_delegate->client());
            }
            mutex().unlock();
        }
    }
}

void VncListenerThread::setRfbClient(RfbClientDelegate *val){
    m_delegate = val;
}
