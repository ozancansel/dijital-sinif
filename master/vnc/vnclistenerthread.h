#ifndef VNCLISTENERTHREAD_H
#define VNCLISTENERTHREAD_H

#include <QThread>
#include <QMutex>

class RfbClientDelegate;

class VncListenerThread : public QThread
{

public:

    VncListenerThread(QObject* parent = nullptr);
    static QMutex&      mutex();
    void    setRfbClient(RfbClientDelegate* val);

protected:

    void    run();

private:

    RfbClientDelegate*  m_delegate;
    static QMutex       m_mutex;

};

#endif // VNCLISTENERTHREAD_H
