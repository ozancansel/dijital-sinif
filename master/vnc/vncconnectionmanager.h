#ifndef VNCCONNECTIONMANAGER_H
#define VNCCONNECTIONMANAGER_H

#include <QQuickItem>
#include <QHash>
#include <QPixmap>
#include "rfbclientdelegate.h"
#include "vnclistenerthread.h"

// RFB related
#define QVNCVIEWER_BITS_PER_SAMPLE		8
#define QVNCVIEWER_SAMPLES_PER_PIXEL		1
#define QVNCVIEWER_BYTES_PER_PIXEL		4
#define QVNCVIEWER_VNC_BASE_PORT		5900

class VncConnectionManager : public QQuickItem
{

    Q_OBJECT

public:

    static void     registerQmlType();
    VncConnectionManager(QQuickItem *parent = nullptr);

    static rfbBool  rfbResize(rfbClient* client);
    static void     rfbUpdate(rfbClient* client , int x , int y , int w, int h);
    static void     rfbUpdateFinished(rfbClient* client);
    static uint32_t m_scanlineWidth;
    static QHash<rfbClient* , QList<QPixmap>> m_updatePixmaps;
    static QHash<rfbClient* , QList<QRect>> m_updateRects;
    static RfbClientDelegate*          getDelegate(rfbClient* client);
    static QList<RfbClientDelegate*>   m_delegates;

public slots:

    void            connectTo(QString url);

signals:

    void            newConnection(RfbClientDelegate* delegate);
    void            connectionFailure(QString message);
    void            connectionSuccess(QString message);

private:

    void            requestFullFrameBufferUpdate(rfbClient* client);

};

#endif // VNCCONNECTIONMANAGER_H
