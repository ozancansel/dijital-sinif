#include "rfbclientdelegate.h"
#include <QDebug>

void RfbClientDelegate::registerQmlType(){
    qmlRegisterType<RfbClientDelegate>("DijitalSinif" , 1 , 0 , "RfbClientDelegate");
}

RfbClientDelegate::RfbClientDelegate(QQuickItem* parent) :
    QQuickItem(parent) ,
    m_client(nullptr)
{   }

rfbClient* RfbClientDelegate::client(){
    return m_client;
}

void RfbClientDelegate::setRfbClient(rfbClient *client){
    m_client = client;

    if(client){
        m_listener_thread.setRfbClient(this);
        m_listener_thread.start();
    }
}

void RfbClientDelegate::sendUpdateSignal(){
    emit updateFrame();
}


QList<QPixmap>& RfbClientDelegate::updatePixmaps(){
    return m_updatePixmaps;
}

QList<QRect>& RfbClientDelegate::updateRects(){
    return m_updateRects;
}
