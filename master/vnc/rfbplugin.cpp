#include "rfbplugin.h"
#include "rfbclientdelegate.h"
#include "vncwidget.h"
#include "vncconnectionmanager.h"
#include <QtQml>

void rfbPlugin::attach(){
    RfbClientDelegate::registerQmlType();
    VncWidget::registerQmlType();
    VncConnectionManager::registerQmlType();
}
