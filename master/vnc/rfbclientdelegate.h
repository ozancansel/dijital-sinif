#ifndef RFBCLIENTDELEGATE_H
#define RFBCLIENTDELEGATE_H

#include <QQuickItem>
#include <QPixmap>
#include <rfb/rfbclient.h>
#include "vnclistenerthread.h"

class RfbClientDelegate : public QQuickItem
{

    Q_OBJECT

public:

    static void         registerQmlType();
    RfbClientDelegate(QQuickItem* parent = nullptr);
    rfbClient*      client();
    QList<QPixmap> &updatePixmaps();
    QList<QRect>   &updateRects();
    void            setRfbClient(rfbClient* client);
    void            sendUpdateSignal();

signals:

    void            updateFrame();

private:

    rfbClient*          m_client;
    QList<QPixmap>      m_updatePixmaps;
    QList<QRect>        m_updateRects;
    VncListenerThread   m_listener_thread;

};

#endif // RFBCLIENTDELEGATE_H
