#include "vncconnectionmanager.h"
#include <QImage>

uint32_t VncConnectionManager::m_scanlineWidth  =   0;
QList<RfbClientDelegate*>   VncConnectionManager::m_delegates;

void VncConnectionManager::registerQmlType(){
    qmlRegisterType<VncConnectionManager>("DijitalSinif" , 1 , 0 , "VncConnectionManager");
}

VncConnectionManager::VncConnectionManager(QQuickItem* parent)  :
    QQuickItem(parent)
{

}

void VncConnectionManager::connectTo(QString url){

    //Client olusturuluyor
    rfbClient*  client = nullptr;
    client = rfbGetClient(QVNCVIEWER_BITS_PER_SAMPLE , QVNCVIEWER_SAMPLES_PER_PIXEL , QVNCVIEWER_BYTES_PER_PIXEL);
    client->MallocFrameBuffer =  rfbResize;
    client->GotFrameBufferUpdate = rfbUpdate;
    client->FinishedFrameBufferUpdate =  rfbUpdateFinished;
    client->programName = url.toLatin1();
    client->frameBuffer = nullptr;
    client->canHandleNewFBSize = true;
    client->canUseCoRRE = true;
    client->canUseHextile = true;
    client->appData.forceTrueColour = true;
    client->appData.useRemoteCursor = true;
    client->appData.enableJPEG = true;

    //Vnc serverina baglaniliyor
    if(ConnectToRFBServer(client , url.toLocal8Bit().constData() , 5900)){
        if(InitialiseRFBConnection(client)){
            client->appData.encodingsString = "Hextile";
            client->appData.qualityLevel = 9;
            client->appData.compressLevel = 6;

            if(!SetFormatAndEncodings(client)){

            }

            client->width = client->si.framebufferWidth;
            client->height = client->si.framebufferHeight;
            client->frameBuffer = (uint8_t *)malloc(client->width * client->height * QVNCVIEWER_BYTES_PER_PIXEL);
            m_scanlineWidth = client->width * QVNCVIEWER_BYTES_PER_PIXEL;
            RfbClientDelegate*  delegate = new RfbClientDelegate(this);
            delegate->setRfbClient(client);
            m_delegates << delegate;

            requestFullFrameBufferUpdate(client);
            emit newConnection(delegate);

//            while(true){
//                if(WaitForMessage(client , 5000)){
//                    HandleRFBServerMessage(client);
//                }
//            }
        } else {
            ::close(client->sock);
            rfbClientCleanup(client);
        }
    } else {
        rfbClientCleanup(client);
    }
}

rfbBool VncConnectionManager::rfbResize(rfbClient *client){
//    uint8_t* oldBuffer = client->frameBuffer;
//    client->frameBuffer = (uint8_t)malloc(client->width * client->height * QVNCVIEWER_BYTES_PER_PIXEL);
//    m_scanlineWidth = client->width * QVNCVIEWER_BYTES_PER_PIXEL;

//    if(oldBuffer)
//        free(oldBuffer);
    return 5;
}

void VncConnectionManager::rfbUpdate(rfbClient *client, int x, int y, int w, int h){
    QImage  image(w , h , QImage::Format_ARGB32);
    int x_max = x + w;
    int y_max = y + h;

    for (int yy = y; yy < y_max; yy++) {
        qint32 yy_times_scanline_width = yy * m_scanlineWidth;
        qint32 yy_normalized = yy - y;
        for (int xx = x; xx < x_max; xx++) {
            qint32 pos = yy_times_scanline_width + xx * QVNCVIEWER_BYTES_PER_PIXEL;
            image.setPixel(xx - x, yy_normalized, qRgb((quint8)client->frameBuffer[pos + 0], (quint8)client->frameBuffer[pos + 1], (quint8)client->frameBuffer[pos + 2]));
        }
    }

    RfbClientDelegate*  delegate = getDelegate(client);

    if(delegate){
        qDebug() << "New image";
        delegate->updatePixmaps() << QPixmap::fromImage(image);
        delegate->updateRects() << QRect(x , y , w , h);
//        delegate->m_updatePixmaps
//        delegate->m_updateRects << QRect(x, y, w, h);
    }
}

void VncConnectionManager::rfbUpdateFinished(rfbClient *client){
    //MainWindow::log("ConnectionWindow::rfbUpdateFinished()");
    //    ConnectionWindow *connectionWindow = clientWindow(client);
    RfbClientDelegate*  delegate = getDelegate(client);
    if(delegate){
        delegate->sendUpdateSignal();
    }
    if ( qApp->hasPendingEvents() )
        qApp->processEvents();
}

RfbClientDelegate* VncConnectionManager::getDelegate(rfbClient *client){
    foreach (RfbClientDelegate* delegate, m_delegates) {
        if(delegate->client() == client)
            return delegate;
    }

    return nullptr;
}

void VncConnectionManager::requestFullFrameBufferUpdate(rfbClient* client){
        client->updateRect.x = client->updateRect.y = 0;
        client->updateRect.w = client->width;
        client->updateRect.h = client->height;
        SendIncrementalFramebufferUpdateRequest(client);
}
