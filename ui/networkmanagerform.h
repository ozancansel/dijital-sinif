#ifndef NETWORKMANAGERFORM_H
#define NETWORKMANAGERFORM_H

#include <QWidget>

namespace Ui {
class NetworkManagerForm;
}

class NetworkManagerForm : public QWidget
{
    Q_OBJECT

public:
    explicit NetworkManagerForm(QWidget *parent = 0);
    ~NetworkManagerForm();

private:
    Ui::NetworkManagerForm *ui;
};

#endif // NETWORKMANAGERFORM_H
