#include "networkmanagerform.h"
#include "ui_networkmanagerform.h"

NetworkManagerForm::NetworkManagerForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::NetworkManagerForm)
{
    ui->setupUi(this);
}

NetworkManagerForm::~NetworkManagerForm()
{
    delete ui;
}
