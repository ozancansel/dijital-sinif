#include "pcscanthread.h"

PcScanThread::PcScanThread()
{
    qRegisterMetaType<QList<QSharedPointer<PcInfo> > >();
}

void PcScanThread::run(){
    emit scanStarted();
    PcScan  scanner;

    QList<QSharedPointer<PcInfo>> computers = scanner.scanComputers();

    emit scanFinished(computers);
}
