#ifndef PCSCANTHREAD_H
#define PCSCANTHREAD_H

#include <QThread>
#include "pcscan.h"
#include <QSharedPointer>

class PcScanThread : public QThread
{

    Q_OBJECT

public:

    PcScanThread();

signals:

    void    scanStarted();
    void    scanFinished(QList<QSharedPointer<PcInfo>>  computers);

protected:

    void    run();

};

#endif // PCSCANTHREAD_H
