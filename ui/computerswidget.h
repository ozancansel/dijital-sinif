#ifndef COMPUTERSWIDGET_H
#define COMPUTERSWIDGET_H

#include <QWidget>
#include <QSharedPointer>
#include "pcinfo.h"

namespace Ui {
class ComputersWidget;
}

class ComputersWidget : public QWidget
{
    Q_OBJECT

public:

    explicit ComputersWidget(QWidget *parent = 0);
    ~ComputersWidget();

public slots:

    void    refreshComputers(bool enabled);
    void    scanComputersFinished(QList<QSharedPointer<PcInfo>> computers);
    void    scanComputersStarted();

private:

    Ui::ComputersWidget *ui;
    QList<QSharedPointer<ComputerData>>  m_computers;
    QSharedPointer<PcInfo>      computerExists(const QString &macAddress);
    void                        addComputer(QSharedPointer<PcInfo>  computer);


};

#endif // COMPUTERSWIDGET_H
