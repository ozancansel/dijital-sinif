#include "computerswidget.h"
#include "ui_computerswidget.h"

ComputersWidget::ComputersWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ComputersWidget)
{
    ui->setupUi(this);
}

ComputersWidget::~ComputersWidget()
{
    delete ui;
}


//Slots
void ComputersWidget::refreshComputers(bool enabled){
    Q_UNUSED(enabled)

    PcScanThread* scanThread = new PcScanThread();
    connect(scanThread , SIGNAL(scanFinished(QList<QSharedPointer<PcInfo>>)) , this , SLOT(scanComputersFinished(QList<QSharedPointer<PcInfo>>)));
    connect(scanThread , SIGNAL(scanStarted()) , this , SLOT(scanComputersStarted()));
    connect(scanThread , SIGNAL(finished()), scanThread , SLOT(deleteLater()));

    //Tarama islemi baslatiliyor
    scanThread->start();
}

void ComputersWidget::scanComputersStarted(){
    m_scanning = true;
    ui->statusBar->showMessage("Bilgisayarlar taraniyor..." , m_messageTimeout);
}

void ComputersWidget::scanComputersFinished(QList<QSharedPointer<PcInfo>> computers){
    m_scanning = false;
    ui->statusBar->showMessage("Tarama islemi sona erdi." , m_messageTimeout);

    foreach (QSharedPointer<PcInfo> computer, computers) {
        QSharedPointer<PcInfo> existingComputer = computerExists(computer.data()->macAddress);

        //Eger ki bilgisayar listede mevcutsa
        if(!existingComputer.isNull())  continue;

        addComputer(computer);
        addComputer(computer);
        addComputer(computer);
        addComputer(computer);

    }
}

QSharedPointer<PcInfo> ComputersWidget::computerExists(const QString &macAddress){

    //Bilgisayar listede varmi diye kontrol ediliyor
    foreach(QSharedPointer<ComputersWidget::ComputerData> data , m_computers.list()){
        if(data.data()->info.data()->macAddress == macAddress){
            return data.data()->info;
        }
    }

    return QSharedPointer<PcInfo>(nullptr);
}

void ComputersWidget::addComputer(QSharedPointer<PcInfo> computer){

    ComputersWidget::ComputerData*   data = new ComputerData();

    NodeItem *widget = new NodeItem(this);
    data->info = computer;
    data->widget = widget;

    m_computers.add(QSharedPointer<ComputersWidget::ComputerData>(data));
}
