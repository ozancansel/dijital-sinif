#include "pcscan.h"
#include <QProcess>
#include <QRegularExpression>
#include <QRegularExpressionMatch>
#include <QRegularExpressionMatchIterator>
#include <QDebug>

PcScan::PcScan(){

}

QList<QSharedPointer<PcInfo>>   PcScan::scanComputers(){

    QStringList interfaces = scanInterfaces();
    interfaces.removeOne("lo"); //lo ignore ediliyor

    QList<QSharedPointer<PcInfo>>   computers;

    foreach (QString interface, interfaces) {

        QProcess    proc;

        proc.start(QString("sudo arp-scan --interface=%0 --localnet").arg(interface));
        bool finished= proc.waitForFinished(5000);

        //Eger islem yarida kesildiyse
        if(!finished){
            return QList<QSharedPointer<PcInfo>>();
        }

        QString result(proc.readAllStandardOutput());
        QRegularExpression  regxr("((\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3}))\\s(((\\d|[a-fA-F]){2})\\:((\\d|[a-fA-F]){2})\\:((\\d|[a-fA-F]){2})\\:((\\d|[a-fA-F]){2})\\:((\\d|[a-fA-F]){2})\\:((\\d|[a-fA-F]){2}))\\s(.*)");

        QRegularExpressionMatchIterator iter = regxr.globalMatch(result);

        while(iter.hasNext()){
            QRegularExpressionMatch match = iter.next();

#ifdef QT_DEBUG
            qDebug() << " Interface : " << interface << "Ip : " << match.captured(1) << " Addr : " << match.captured(6) << " Pc Name : " << match.captured(19);
#endif
            PcInfo*  inf = new PcInfo();
            inf->ip = match.captured(1);
            inf->macAddress = match.captured(6);
            inf->name = match.captured(19);
            inf->interface = interface;

            computers.append(QSharedPointer<PcInfo>(inf));
        }
    }

    return computers;
}

QStringList PcScan::scanInterfaces(){
    QProcess    proc;

    proc.start("ip link show");
    bool finished = proc.waitForFinished(5000);

    if(!finished){
        return QStringList();
    }


    //Komut ciktisi okunuyor
    QString result(proc.readAllStandardOutput());

    QRegularExpression  regex("\\d\\:\\s(([a-zA-Z]|\\d)+)\\:");

    QRegularExpressionMatchIterator iter = regex.globalMatch(result);

    QStringList interfaces;
    while(iter.hasNext()){
        QRegularExpressionMatch match = iter.next();

#ifdef QT_DEBUG
        qDebug() << "Interface -> " << match.captured(1);
#endif
        interfaces << match.captured(1);
    }

    return interfaces;
}
