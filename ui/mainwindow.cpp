#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "nodeitem.h"
#include "observablecollection.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow) ,
    m_messageTimeout(5000) ,
    m_scanning(false)
{
    ui->setupUi(this);
    connect(ui->actionYenile , SIGNAL(triggered(bool)) , this , SLOT(refreshComputers(bool)));
}

MainWindow::~MainWindow()
{
    delete ui;
}
