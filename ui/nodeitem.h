#ifndef NODEITEM_H
#define NODEITEM_H

#include <QWidget>

namespace Ui {
class NodeItem;
}

class NodeItem : public QWidget
{
    Q_OBJECT

public:

    enum States{ Open = 0, Close = 1 };
    explicit NodeItem(QWidget *parent = 0);
    ~NodeItem();

private:

    Ui::NodeItem *ui;

    States      m_state;
    void        setState(States state);

};

#endif // NODEITEM_H
