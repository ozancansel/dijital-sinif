#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "thread/pcscanthread.h"
#include "nodeitem.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{

public:

    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();


private:

    struct ComputerData{
        QSharedPointer<PcInfo>  info;
        NodeItem*               widget;
    };

    Ui::MainWindow              *ui;
    const int                   m_messageTimeout;
    bool                        m_scanning;


};

#endif // MAINWINDOW_H
