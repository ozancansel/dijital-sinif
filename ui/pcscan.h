#ifndef PCSCAN_H
#define PCSCAN_H

#include <QString>
#include <QMetaType>
#include <QSharedPointer>
#include "pcinfo.h"

class PcScan    {

public:

    PcScan();

    QList<QSharedPointer<PcInfo>>   scanComputers();
    QStringList                     scanInterfaces();

private:

    QString     m_storage;

};

#endif // PCSCAN_H
