#ifndef PCINFO_H
#define PCINFO_H

#include <QString>
#include <QObject>

class PcInfo : public QObject
{

    Q_OBJECT

public:

    QString     macAddress;
    QString     name;
    QString     ip;
    QString     interface;

};

#endif // PCINFO_H
