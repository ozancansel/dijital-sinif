#include "nodeitem.h"
#include "ui_nodeitem.h"

NodeItem::NodeItem(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::NodeItem) ,
    m_state(Close)
{
    ui->setupUi(this);
    setState(Open);
}

NodeItem::~NodeItem()
{
    delete ui;
}

void NodeItem::setState(States state){
    switch(state){
        case Open:
            ui->stateImage->setPixmap(QPixmap(":/res/img/opened-pc-icon.png"));
        break;
        case Close:
            ui->stateImage->setPixmap(QPixmap(":/res/img/closed-pc-icon.png"));
        break;
    }
}
